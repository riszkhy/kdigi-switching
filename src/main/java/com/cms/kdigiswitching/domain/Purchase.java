package com.cms.kdigiswitching.domain;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value=Include.NON_NULL)
public class Purchase {
	private String productcode;
	private String amount;
	private String msisdn;
	private String counter;
	private String dateoftrx;
	
	//Additional param respon Purchase
	private String rc;
	private String description;
	private String resi;
	
	
	
}
