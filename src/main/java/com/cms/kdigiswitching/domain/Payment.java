package com.cms.kdigiswitching.domain;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(value=Include.NON_NULL)
public class Payment {
	private String productcode;
	private String idpelanggan;
	private String idalternative;
	private String dateoftrx;
	private String tipetrx;
	private String nama;
	private String nominal;
	private String jumlahlembar;
	private String biayaadmin;
	private String kodereffkopnus;
	private String kodereffbiler;
	//Additional pay respon
	private String rc;
	private String description;
	private String resi;
}
