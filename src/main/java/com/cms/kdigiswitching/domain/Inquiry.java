package com.cms.kdigiswitching.domain;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value=Include.NON_NULL)
public class Inquiry {
	private String productcode;
	private String idpelanggan;
	private String idalternative;
	private String dateoftrx;
	
	//additional bit inq respon
	private String rc;
	private String description;
	private String tipetrx;
	private String nama;
	private String nominal;
	private String jumlahlembar;
	private String biayaadmin;
	private String kodereffkopnus;
	private String kodereffbiler;
	private String screen;
	
	
	
}
