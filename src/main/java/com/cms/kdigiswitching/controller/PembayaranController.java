package com.cms.kdigiswitching.controller;

import java.util.Date;

import org.jpos.iso.ISODate;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.jpos.util.NameRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cms.kdigiswitching.dict.Bit48Payment;
import com.cms.kdigiswitching.domain.Inquiry;
import com.cms.kdigiswitching.domain.Payment;
import com.cms.kdigiswitching.util.AppProperties;
import com.cms.kdigiswitching.util.MidEndStringUtil;
import com.cms.kdigiswitching.util.Utility;

@RestController
@RequestMapping("/sw")
public class PembayaranController {
	private int timeout;
	@Autowired
	private AppProperties prop;
	@PostMapping(value="/inquiry")
	public Inquiry getInquiry(@RequestBody Inquiry inquiry) {
		Inquiry inq = inquiry;
		try{
			System.out.println(prop.getTimeout());
			timeout = Integer.valueOf(prop.getTimeout());
			Date d = new Date();
			ISOMsg msg = new ISOMsg();
			msg.setMTI("0200"); 
	        msg.set(new ISOField(2, MidEndStringUtil.SpaceOrNol("0", 19)));
	        msg.set(new ISOField(3, "380000"));
	        msg.set(new ISOField(4, MidEndStringUtil.SpaceOrNol("0", 12)));
	        msg.set(new ISOField(7, ISODate.formatDate(d, "MMddHHmmss") ));
	        String sBit11out = "";
	
	        sBit11out = String.valueOf(Math.round(Math.random() * 1000000));
	        String sBit37 = ISODate.formatDate(d, "HHmmss") + MidEndStringUtil.SpaceOrNol(sBit11out, 6);
           msg.set(new ISOField(11, MidEndStringUtil.SpaceOrNol(sBit11out, 6)));
          
           
           msg.set(new ISOField(12, ISODate.formatDate(d, "HHmmss")));
           msg.set(new ISOField(13, ISODate.formatDate(d, "MMdd")));
           msg.set(new ISOField(15, ISODate.formatDate(d, "MMdd")));
           msg.set(new ISOField(18, "7011")); // teller
           msg.set(new ISOField(32, prop.getCacode()));	// Product ID Internal 
           msg.set(new ISOField(37, sBit37));
           msg.set(new ISOField(41, "0")); //length = 1
           msg.set(new ISOField(42, "KDIGI"));
         
           StringBuffer sBit48Send = new StringBuffer();
           sBit48Send.append(Utility.SpaceOrCut(inq.getProductcode(), 7))
   			 		 .append(Utility.SpaceOrCut(inq.getIdpelanggan(), 20))
   			 		 .append(Utility.SpaceOrNol(inq.getIdalternative(), 20));
           
           msg.set(new ISOField(48, sBit48Send.toString())); //no Account
           msg.set(new ISOField(49, "360"));
           msg.set(new ISOField(63,inq.getProductcode().substring(0, 2)));
           
           MUX mux = (MUX) NameRegistrar.getIfExists("mux.jpos-client-mux");
           ISOMsg msgresp = mux.request(msg, timeout * 1000);
           System.out.println(new String(msgresp.pack()));
           inq.setRc(msgresp.getString(39));
           if(inq.getRc().equals("00")) {
        	   inq = new Bit48Payment().inqresp(inq, msgresp.getString(48));
           }
           inq.setDescription("-");
           inq.setScreen(msgresp.getString(61).concat((msgresp.getString(62)!=null)?msgresp.getString(62):""));
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			inq.setRc("99");
	        inq.setDescription("Failed");
		}
		return inq;
	}
	@PostMapping(value="/payment")
	public Payment getPay(@RequestBody Payment payment) {
		Payment pay = payment;
		try{
			System.out.println(prop.getTimeout());
			timeout = Integer.valueOf(prop.getTimeout());
			Date d = new Date();
			ISOMsg msg = new ISOMsg();
			msg.setMTI("0200"); 
	        msg.set(new ISOField(2, MidEndStringUtil.SpaceOrNol("0", 19)));
	        msg.set(new ISOField(3, "180000"));
	        msg.set(new ISOField(4, MidEndStringUtil.SpaceOrNol(String.valueOf(Long.valueOf(pay.getNominal()) * 100), 12)));
	        msg.set(new ISOField(7, ISODate.formatDate(d, "MMddHHmmss") ));
	        String sBit11out = "";
	
	        sBit11out = String.valueOf(Math.round(Math.random() * 1000000));
	        String sBit37 = ISODate.formatDate(d, "HHmmss") + MidEndStringUtil.SpaceOrNol(sBit11out, 6);
           msg.set(new ISOField(11, MidEndStringUtil.SpaceOrNol(sBit11out, 6)));
          
           
           msg.set(new ISOField(12, ISODate.formatDate(d, "HHmmss")));
           msg.set(new ISOField(13, ISODate.formatDate(d, "MMdd")));
           msg.set(new ISOField(15, ISODate.formatDate(d, "MMdd")));
           msg.set(new ISOField(18, "7011")); // teller
           msg.set(new ISOField(32, prop.getCacode()));	// Product ID Internal 
           msg.set(new ISOField(37, sBit37));
           msg.set(new ISOField(41, "0")); //length = 1
           msg.set(new ISOField(42, "KDIGI"));
         
           StringBuffer sBit48Send = new StringBuffer();
           StringBuffer sSign = new StringBuffer();
           sSign.append(Utility.SpaceOrCut(pay.getProductcode(), 7))
	   			.append(Utility.SpaceOrCut(pay.getIdpelanggan(), 20))
	   			.append(Utility.SpaceOrCut(pay.getIdalternative(), 20))
	   			.append(Utility.SpaceOrCut(pay.getNama(), 30))
	   			.append(Utility.SpaceOrNol(pay.getNominal(), 12))
	   			.append(Utility.SpaceOrNol(pay.getBiayaadmin(), 10))
	   			.append(Utility.SpaceOrCut(pay.getKodereffkopnus(), 20))
	   			.append(Utility.SpaceOrCut(pay.getKodereffbiler(), 32))
	   			.append(prop.getUser())
	   			.append(Utility.enCryptor("MD5", prop.getPass()));
           // signature
           sBit48Send.append(Utility.SpaceOrCut(pay.getProductcode(), 7))
  					.append(Utility.SpaceOrCut(pay.getIdpelanggan(), 20))
  					.append(Utility.SpaceOrCut(pay.getIdalternative(), 20))
  					.append(Utility.SpaceOrCut(pay.getTipetrx(), 1))
  					.append(Utility.SpaceOrCut(pay.getNama(), 30))
  					.append(Utility.SpaceOrNol(pay.getNominal(), 12))
  					.append(Utility.SpaceOrNol(pay.getBiayaadmin(), 10))
  					.append(Utility.SpaceOrCut(pay.getKodereffkopnus(), 20))
  					.append(Utility.SpaceOrCut(pay.getKodereffbiler(), 32))
           			.append(Utility.SpaceOrCut(Utility.enCryptor("SHA1", sSign.toString()), 40));
           
           msg.set(new ISOField(48, sBit48Send.toString())); //no Account
           msg.set(new ISOField(49, "360"));
           msg.set(new ISOField(63,pay.getProductcode().substring(0, 2)));
           
           MUX mux = (MUX) NameRegistrar.getIfExists("mux.jpos-client-mux");
           ISOMsg msgresp = mux.request(msg, timeout * 1000);
           System.out.println(new String(msgresp.pack()));
           pay.setRc(msgresp.getString(39));
           pay.setDescription("-");
           pay.setResi(msgresp.getString(61).concat((msgresp.getString(62)!=null)?msgresp.getString(62):""));
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			pay.setRc("99");
	        pay.setDescription("Failed");
		}
		return pay;
	}
}
