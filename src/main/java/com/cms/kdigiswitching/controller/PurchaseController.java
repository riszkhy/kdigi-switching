package com.cms.kdigiswitching.controller;

import java.util.Date;
import java.util.Properties;

import org.jpos.iso.ISODate;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.jpos.util.NameRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.kdigiswitching.domain.Purchase;
import com.cms.kdigiswitching.util.AppProperties;
import com.cms.kdigiswitching.util.MidEndStringUtil;
import com.cms.kdigiswitching.util.Utility;

@RestController
@RequestMapping("/sw")
public class PurchaseController {

	@Autowired
	private AppProperties prop;
	private int timeout = 0;
	@PostMapping(value="/purchase")
	@ResponseBody
	public Purchase purchase(@RequestBody Purchase purchase) {
		Purchase purc = purchase;
		try{
			System.out.println(prop.getTimeout());
			timeout = Integer.valueOf(prop.getTimeout());
			Date d = new Date();
			ISOMsg msg = new ISOMsg();
			msg.setMTI("0200"); 
	        msg.set(new ISOField(2, MidEndStringUtil.SpaceOrNol("0", 19)));
	        msg.set(new ISOField(3, "170000"));
	        msg.set(new ISOField(4, MidEndStringUtil.SpaceOrNol(String.valueOf(Long.valueOf(purc.getAmount()) * 100), 12)));
	        msg.set(new ISOField(7, ISODate.formatDate(d, "MMddHHmmss") ));
	        String sBit11out = "";
	
	        sBit11out = String.valueOf(Math.round(Math.random() * 1000000));
	        String sBit37 = ISODate.formatDate(d, "HHmmss") + MidEndStringUtil.SpaceOrNol(sBit11out, 6);
           msg.set(new ISOField(11, MidEndStringUtil.SpaceOrNol(sBit11out, 6)));
          
           
           msg.set(new ISOField(12, ISODate.formatDate(d, "HHmmss")));
           msg.set(new ISOField(13, ISODate.formatDate(d, "MMdd")));
           msg.set(new ISOField(15, ISODate.formatDate(d, "MMdd")));
           msg.set(new ISOField(18, "7011")); // teller
           msg.set(new ISOField(32, prop.getCacode()));	// Product ID Internal 
           msg.set(new ISOField(37, sBit37));
           msg.set(new ISOField(41, "0")); //length = 1
           msg.set(new ISOField(42, "KDIGI"));
         
           StringBuffer sBit48Send = new StringBuffer();
           StringBuffer sSign = new StringBuffer();
           sSign.append(Utility.SpaceOrCut(purc.getProductcode(), 7))
	   			 .append(Utility.SpaceOrCut(purc.getMsisdn(), 20))
	   			 .append(Utility.SpaceOrNol(purc.getCounter(), 2))
	   			 .append(Utility.SpaceOrCut(prop.getUser(), 20))
	   			 .append(Utility.SpaceOrCut(Utility.enCryptor("MD5", prop.getPass()), 40));
           // signature
           sBit48Send.append(Utility.SpaceOrCut(purc.getProductcode(), 7))
   			 		 .append(Utility.SpaceOrCut(purc.getMsisdn(), 20))
   			 		 .append(Utility.SpaceOrNol(purc.getCounter(), 2))
           			 .append(Utility.SpaceOrCut(prop.getUser(), 20))
           			 .append(Utility.SpaceOrCut(Utility.enCryptor("SHA1", sSign.toString()), 40));
           
           msg.set(new ISOField(48, sBit48Send.toString())); //no Account
           msg.set(new ISOField(49, "360"));
           msg.set(new ISOField(63,purc.getProductcode().substring(0, 2)));
           
           MUX mux = (MUX) NameRegistrar.getIfExists("mux.jpos-client-mux");
           ISOMsg msgresp = mux.request(msg, timeout * 1000);
           System.out.println(new String(msgresp.pack()));
           purc.setRc(msgresp.getString(39));
           purc.setDescription(msgresp.getString(61));
           purc.setResi(msgresp.getString(61).concat((msgresp.getString(62)!=null)?msgresp.getString(62):""));
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			purc.setRc("99");
	        purc.setDescription("Failed");
		}
		return purc;
	}
	
}
