package com.cms.kdigiswitching.dict;

import com.cms.kdigiswitching.domain.Inquiry;
import com.cms.kdigiswitching.util.Cutter;

public class Bit48Payment {
	public Inquiry inqresp(Inquiry inq,String bit48) {
		Cutter cut = new Cutter();
		inq.setProductcode(cut.consume(bit48, 7));
		inq.setIdpelanggan(cut.consume(bit48, 20));
		inq.setIdalternative(cut.consume(bit48, 20));
		inq.setTipetrx(cut.consume(bit48, 1));
		inq.setNama(cut.consume(bit48, 30));
		inq.setNominal(cut.consume(bit48, 12));
		inq.setJumlahlembar(cut.consume(bit48, 2));
		inq.setBiayaadmin(cut.consume(bit48, 10));
		inq.setKodereffkopnus(cut.consume(bit48, 20));
		inq.setKodereffbiler(cut.consume(bit48, 32));
		return inq;
	}
}
