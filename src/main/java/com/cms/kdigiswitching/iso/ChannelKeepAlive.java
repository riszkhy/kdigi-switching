package com.cms.kdigiswitching.iso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;


public class ChannelKeepAlive extends Thread
{
  private static final Log logs = LogFactory.getLog(ChannelKeepAlive.class);
  private int reconnectDelay = 0;
  private MUX mux = null;
  private ISOChannel channel= null;
  private Boolean startListening = Boolean.valueOf(false);
  private Boolean LastSendStatus = Boolean.valueOf(false);

  public ChannelKeepAlive(MUX mux, int reconnectDelay, ISOChannel channel )
  {
    this.mux = mux;
    this.reconnectDelay = reconnectDelay;
    this.channel = channel;
  }

  @Override
public void run()
  {
    try {
      this.startListening = Boolean.valueOf(true);
      while (this.startListening.booleanValue()) {
        if (this.mux.isConnected())
          sendMessage(echoTest());
        Thread.sleep(this.reconnectDelay);
//        this.mux.showCounters( System.out) ;
      }
      
    }
    catch (InterruptedException ex) {
      ex.printStackTrace();
    }
  }

  private void sendMessage(ISOMsg msg) {
    ISOMsg msgReceive = null;
    try
    {
      logs.info(new String(msg.pack()));
      logISOMsg(msg);
      msgReceive = this.mux.request(msg, this.reconnectDelay);
      logISOMsg(msgReceive);
    }
    catch (ISOException e) {
      e.printStackTrace();
    } catch (NullPointerException e2) {
      e2.printStackTrace();
    }
    if (msgReceive != null)
      this.LastSendStatus = Boolean.valueOf(true);
    else
      this.LastSendStatus = Boolean.valueOf(false);
  }

  private ISOMsg echoTest()
  {
    ISOMsg msg = new ISOMsg();
    try
    {
      DateFormat dateFormatBit7 = new SimpleDateFormat("MMddHHmmss");

      msg.setPackager(this.channel.getPackager());
      msg.setMTI("0800");
      msg.set(7, dateFormatBit7.format(new Date()));
      msg.set(11,getRetrvNumber(6));
      msg.set(70, "001");
    }
    catch (ISOException e) {
      e.printStackTrace();
    }
    return msg;
  }
  
  public static String getRetrvNumber(int length) 
	{
	    Random random	= new Random();
	    char[] digits	= new char[length];
	    
	    digits[0] = (char) (random.nextInt(9) + '1');
	    
	    for (int i = 1; i < length; i++) 
	    {
	        digits[i] = (char) (random.nextInt(10) + '0');
	    }
	    
	    return String.valueOf(digits);
	}
  
  
  

  public void stopListening() {
    this.startListening = Boolean.valueOf(false);
  }

  public Boolean getLastSendStatus() {
    return this.LastSendStatus;
  }
  
  private static void logISOMsg(ISOMsg msg) {

		System.out.println("----ISO MESSAGE-----");

		try {

			System.out.println("  MTI : " + msg.getMTI());

			for (int i=1;i<=msg.getMaxField();i++) {

				if (msg.hasField(i)) {

					System.out.println("    Field-"+i+" : "+msg.getString(i));

				}

			}

		} catch (ISOException e) {

			e.printStackTrace();

		} finally {

			System.out.println("--------------------");

		}



	}
}
