package com.cms.kdigiswitching.iso;


import java.io.IOException;
import java.net.ServerSocket;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;

public class SwitchAsciiChannel extends BaseChannel {
  public SwitchAsciiChannel () {
      super();
  }
  public SwitchAsciiChannel (String host, int port, ISOPackager p) {
      super(host, port, p);
  }
  /**
   * Construct server ISOChannel
   * @param p     an ISOPackager
   * @exception IOException
   * @see ISOPackager
   */
  public SwitchAsciiChannel (ISOPackager p) throws IOException {
      super(p);
  }
  /**
   * constructs a server ISOChannel associated with a Server Socket
   * @param p     an ISOPackager
   * @param serverSocket where to accept a connection
   * @exception IOException
   * @see ISOPackager
   */
  public SwitchAsciiChannel (ISOPackager p, ServerSocket serverSocket) 
      throws IOException
  {
      super(p, serverSocket);
  }
  /**
   * @param len the packed Message len
   * @exception IOException
   */
  protected void sendMessageLength(int len) throws IOException {
  	byte[] b= new byte[2];
//  	b[0]= (byte) ((len+2) % 256);
//  	b[1]=(byte) ((len+2) / 256);
  	b[0]= (byte) (len % 256);
	b[1]=(byte) (len / 256);
  	
//  	len = len + 2;
//    b[0] = (byte) (len / 256);
//    b[1] = (byte) (len % 256);
  	
  	serverOut.write(b);
  }
  /**
   * @return the Message len
   * @exception IOException, ISOException
   */
  protected int getMessageLength() throws IOException, ISOException {
      byte[] c= new byte[2];
      serverIn.readFully(c,0,2);
      
      return (int) (c[0] & 0xff) + (256 * c[1]);
//      return (int) (c[0] * 256 ) + (c[1] & 0xff) - 2;
  }
}


