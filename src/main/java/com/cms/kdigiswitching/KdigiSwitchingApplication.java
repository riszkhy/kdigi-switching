package com.cms.kdigiswitching;

import javax.annotation.PostConstruct;

import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.MUX;
import org.jpos.q2.Q2;
import org.jpos.util.NameRegistrar;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cms.kdigiswitching.iso.ChannelKeepAlive;
import com.cms.kdigiswitching.iso.SwitchAsciiChannel;
import com.cms.kdigiswitching.iso.SwitchPackager;

@SpringBootApplication
public class KdigiSwitchingApplication {

	public static ISOPackager packager;
	public static ISOChannel channel;
	public static void main(String[] args) {
		SpringApplication.run(KdigiSwitchingApplication.class, args);
	}
	
	public void startQ2() {
		Q2 jpos = new Q2();
		jpos.start();
		
	}
	@PostConstruct
	public void app(){
		try{
			startQ2();
			ISOUtil.sleep(3000);
	        packager = new SwitchPackager();
	        channel = new SwitchAsciiChannel(packager);
	        
	       MUX mux = (MUX) NameRegistrar.getIfExists("mux.jpos-client-mux");
	       new ChannelKeepAlive(mux, 30 * 1000,channel).start();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
