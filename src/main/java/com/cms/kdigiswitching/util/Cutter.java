package com.cms.kdigiswitching.util;

public class Cutter 
{
	private int offset = 0; 
	
	public String consume(String str, int len)
	{
		String result;
		
		if(len > 0)
		{
			if(offset >= str.length()-1)
			{
				result = "";
			}
			else if(offset+len <= str.length())
			{
				result = str.substring(offset, offset+len);
				offset += len;
			}
			else
			{
		        result = str.substring(offset, str.length());
		        offset = str.length();
			}
		}
		else
		{
			result = "";
		}
    
		return result;
	}
}
