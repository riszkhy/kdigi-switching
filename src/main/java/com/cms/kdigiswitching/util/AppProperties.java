package com.cms.kdigiswitching.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;
import lombok.NoArgsConstructor;

@Configuration
@PropertySource("classpath:app.properties")
@Data
@NoArgsConstructor
public class AppProperties {
	@Value("${iso.timeout}")
	private String timeout;
	@Value("${channel.user}")
	private String user;
	@Value("${channel.pass}")
	private String pass;
	@Value("${channel.cacode}")
	private String cacode;
}
