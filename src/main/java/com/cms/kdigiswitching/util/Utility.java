package com.cms.kdigiswitching.util;

import java.io.IOException;
import java.io.InputStream;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.json.JSONObject;
import org.apache.logging.log4j.Logger;



public class Utility
{	
	private static final Log logs = LogFactory.getLog(Utility.class);
	public static String SpaceOrNol( String data, int panjang )
	{
    	int tot			= panjang-data.length();
    	String tmpNol	= "";
    	
    	for (int i=1; i<=tot; i++){
    		tmpNol+="0";
    		}
    	 	data		= tmpNol + data;
    	
    	return data;
    }
	
	public static String SpaceOrCut( String data, int panjang )
	{		
	   	int tot		= panjang-data.length();
	   	
	   	for (int i=1;i<=tot;i++)
	   	{
	   		data	+= " ";
	   	}
	   	
	   	if ( tot < 0 ) 
	   		data	= data.substring(0,panjang);
	   
	   	return data;
	}
	
	public static String getValProperty( Class<?> clss, String file, String prop )
    {
    	Properties properties = new Properties(); 
        InputStream stream = clss.getResourceAsStream( file );
        
        try 
        {
			properties.load(stream);
		} 
        catch (IOException e) 
        {
			e.printStackTrace();
		}
        
        return properties.getProperty( prop );
	}

	public static String enCryptor( String type, String str ) 
	{
		byte[] digest = null;
		
		try 
		{
			MessageDigest md	= MessageDigest.getInstance( type );
			digest 				= md.digest( ( str ).getBytes() );			
		} 
		catch ( NoSuchAlgorithmException e ) 
		{
			e.printStackTrace();
		}
		
		return bytes2String( digest ); 
	}
	
	private static String bytes2String( byte[] bytes ) 
	{
	    StringBuilder sb = new StringBuilder();
        
        for ( byte b: bytes ) 
        {
                String hexString = Integer.toHexString( 0x00FF & b );
                sb.append( hexString.length() == 1 ? "0" + hexString : hexString );
        }
 
        return sb.toString();
	}
	
	public static String setPrintDate( String strDate )
	{
		Cutter cutter	= new Cutter();
		Calendar cal	= Calendar.getInstance();
		
		String year		= String.valueOf(cal.get(Calendar.YEAR));
		String month	= cutter.consume(strDate, 2);
		String day		= cutter.consume(strDate, 2);
		String hour		= cutter.consume(strDate, 2);
		String minute	= cutter.consume(strDate, 2);
		String second	= cutter.consume(strDate, 2);
		
		return day + "-" + month + "-" + year + " " + hour + ":" + minute + ":" + second;
	}
	
//	public static String formatPrint(TreeMap<String , String> tm) 
//	{
//		String format		= CMSProperties.PRINT_HEADER;
//		
//		for(Entry<String, String> map : tm.entrySet())
//		{
//			String key		= map.getKey();
//			String value	= map.getValue();
//			
//			format = format.replace("$"+key+"$", value);
//		}
//
//		return format;
//	}
	
	public static String getRetrvNumber(int length) 
	{
	    Random random	= new Random();
	    char[] digits	= new char[length];
	    
	    digits[0] = (char) (random.nextInt(9) + '1');
	    
	    for (int i = 1; i < length; i++) 
	    {
	        digits[i] = (char) (random.nextInt(10) + '0');
	    }
	    
	    return String.valueOf(digits);
	}
	
	public static String getCurrTime()
	{
		Calendar cal				= Calendar.getInstance();
		SimpleDateFormat formatter	= new SimpleDateFormat("HHmmss");
	
		return formatter.format(cal.getTime());
	}
	
	public static String getRandomByTime()
	{
		return getRetrvNumber( 6 ) + getCurrTime();
	}
	
	public static void debugLog( Logger logger, ISOBit isoBit ) 
	{
		logger.info("----------------------------------------");
		
		if(isoBit.getMTI() != null)
			logger.info("MTI:" + isoBit.getMTI());
		
		if(isoBit.getBit2() != null)
			logger.info("bit 2:" + isoBit.getBit2());
		
		if(isoBit.getBit3() != null)
			logger.info("bit 3:" + isoBit.getBit3());
		
		if(isoBit.getBit4() != null)
			logger.info("bit 4:" + isoBit.getBit4());
		
		if(isoBit.getBit7() != null)
			logger.info("bit 7:" + isoBit.getBit7());
		
		if(isoBit.getBit11() != null)
			logger.info("bit 11:" + isoBit.getBit11());
		
		if(isoBit.getBit12() != null)
			logger.info("bit 12:" + isoBit.getBit12());
		
		if(isoBit.getBit13() != null)
			logger.info("bit 13:" + isoBit.getBit13());
		
		if(isoBit.getBit14() != null)
			logger.info("bit 14:" + isoBit.getBit14());
		
		if(isoBit.getBit15() != null)
			logger.info("bit 15:" + isoBit.getBit15());
		
		if(isoBit.getBit18() != null)
			logger.info("bit 18:" + isoBit.getBit18());
		
		if(isoBit.getBit22() != null)
			logger.info("bit 22:" + isoBit.getBit22());
		
		if(isoBit.getBit32() != null)
			logger.info("bit 32:" + isoBit.getBit32());
		
		if(isoBit.getBit35() != null)
			logger.info("bit 35:" + isoBit.getBit35());
		
		if(isoBit.getBit37() != null)
			logger.info("bit 37:" + isoBit.getBit37());
		
		if(isoBit.getBit39() != null)
			logger.info("bit 39:" + isoBit.getBit39());
		
		if(isoBit.getBit41() != null)
			logger.info("bit 41:" + isoBit.getBit41());
		
		if(isoBit.getBit42() != null)
			logger.info("bit 42:" + isoBit.getBit42());
		
		if(isoBit.getBit43() != null)	
			logger.info("bit 43:" + isoBit.getBit43());
		
		if(isoBit.getBit48() != null)
			logger.info("bit 48:" + isoBit.getBit48());
		
		if(isoBit.getBit49() != null)
			logger.info("bit 49:" + isoBit.getBit49());
		
		if(isoBit.getBit52() != null)
			logger.info("bit 52:" + isoBit.getBit52());
		
		if(isoBit.getBit61() != null)
			logger.info("bit 61:" + isoBit.getBit61());
		
		if(isoBit.getBit62() != null)
			logger.info("bit 62:" + isoBit.getBit62());
		
		if(isoBit.getBit63() != null)
			logger.info("bit 63:" + isoBit.getBit63());
		
		if(isoBit.getBit112() != null)
			logger.info("bit 112:" + isoBit.getBit112());
		
		logger.info("----------------------------------------");
	}
	
	public static String listToString(ArrayList<String> arrList, String delimiter) 
	{
		StringBuffer sb	= new StringBuffer();
		int size		= arrList.size();
		int i			= 1;
		
		for (Iterator<String> iterator = arrList.iterator(); iterator.hasNext();) 
		{
			String string = (String) iterator.next();
			
			sb.append(string);
			
			if(i != size)
				sb.append(delimiter);
			
			i++;
		}
		
		return sb.toString();
	}
	
	public static String secureCode(ArrayList<String> arr) 
	{
		StringBuffer sb		= new StringBuffer();
		SimpleDateFormat sf	= new SimpleDateFormat("yyyyMMdd");
		Calendar cal		= Calendar.getInstance();
		String today		= sf.format(cal.getTime());
		
		for (Iterator<String> iterator = arr.iterator(); iterator.hasNext();) 
		{
			String string = (String) iterator.next();
			sb.append(string);
		}
		
		sb.append(today);
		
		return Utility.enCryptor("SHA1", sb.toString());
	}
	
//	public static JSONObject responCodeMsg( String message )
//	{
//		JSONObject json		= new JSONObject();
//		
//		try {
//			
//			String[] arr			= message.split("\\~");
//			String[] arr2			= arr[1].split("\\|");
//			
//			json.put("rc", arr[0]);
//			json.put("data", arr2[0]);
//			
//		} catch (ArrayIndexOutOfBoundsException e) {
//
//			json.put("rc", "99");
//			json.put("data", "CEK KONFIGURASI");
//		
//		}
//		
//		return json;
//	}
	
//	public static JSONObject jsonData(String responCode, String message)
//	{
//		JSONObject json = new JSONObject();
//		
//		json.put("rc", 		responCode);
//		json.put("data", 	message);
//		
//		return json;
//	}
	
	public static String dateFormatID( String date ){
		
		Cutter cut	= new Cutter();
		
		String year		= cut.consume(date, 4);
		String month	= cut.consume(date, 2);
		String day		= cut.consume(date, 2);
		
		return day + "/" + month + "/" + year;
		
	}
	
	public static String timeFormatID( String time ){
		
		Cutter cut		= new Cutter();
		
		String hour		= cut.consume(time, 2);
		String minute	= cut.consume(time, 2);
		String second	= cut.consume(time, 2);
		
		return hour + ":" + minute + ":" + second; 
		
	}

//	public static String formatPrintCopy(TreeMap<String, String> tm) 
//	{
//		String format		= CMSProperties.PRINT_HEADER_COPY;
//		
//		for(Entry<String, String> map : tm.entrySet())
//		{
//			String key		= map.getKey();
//			String value	= map.getValue();
//			
//			format = format.replace("$"+key+"$", value);
//		}
//
//		return format;
//	}
	
	public  String getFieldResponse(Connection conn,String mti,String bit3,String field,String sBit48) throws Exception{
		String hasil=null;
			Statement stat = conn.createStatement();
			String query = "select "+field+" from log_mess210 where "
					+ "mti='"+mti+"' and  bit3='"+bit3+"' "
					+ "and bit39 ='00' and bit48 like '%"+sBit48+"%'";
			logs.info("query field:"+query);
			ResultSet rsf = stat.executeQuery(query);
			if(rsf.next())
			{
				hasil = rsf.getString(field);
			}
			rsf.close();
			stat.close();
		return hasil;
	}
}