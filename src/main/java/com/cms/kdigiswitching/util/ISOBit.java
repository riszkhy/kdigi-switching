package com.cms.kdigiswitching.util;

public class ISOBit 
{
	private String 	bit2;
	private String 	bit3;
	private String 	bit4;
	private String 	bit7;
	private String 	bit11;
	private String 	bit12;
	private String 	bit13;
	private String 	bit14;
	private String 	bit15;
	private String 	bit18;
	private String 	bit22;
	private String 	bit32;
	private String 	bit35;
	private String 	bit37;
	private String 	bit39;
	private String 	bit41;
	private String 	bit42;
	private String 	bit43;
	private String 	bit48;
	private String 	bit49;
	private String 	bit52;
	private String 	bit61;
	private String 	bit62;
	private String 	bit63;
	private String  bit90;
	private String 	bit112;
	private String 	bit113;
	private String 	bit114;
	private String 	bit115;
	private String 	ip;
	private int    	port;
	private String 	strmProp;
	private String 	MTI;
	private	int		timeout;
	
	
	public String getBit2() {
		return bit2;
	}
	public void setBit2(String bit2) {
		this.bit2 = bit2;
	}
	public String getBit3() {
		return bit3;
	}
	public void setBit3(String bit3) {
		this.bit3 = bit3;
	}
	public String getBit4() {
		return bit4;
	}
	public void setBit4(String bit4) {
		this.bit4 = bit4;
	}
	public String getBit7() {
		return bit7;
	}
	public void setBit7(String bit7) {
		this.bit7 = bit7;
	}
	public String getBit11() {
		return bit11;
	}
	public void setBit11(String bit11) {
		this.bit11 = bit11;
	}
	public String getBit12() {
		return bit12;
	}
	public void setBit12(String bit12) {
		this.bit12 = bit12;
	}
	public String getBit13() {
		return bit13;
	}
	public void setBit13(String bit13) {
		this.bit13 = bit13;
	}
	public String getBit14() {
		return bit14;
	}
	public void setBit14(String bit14) {
		this.bit14 = bit14;
	}
	public String getBit15() {
		return bit15;
	}
	public void setBit15(String bit15) {
		this.bit15 = bit15;
	}
	public String getBit18() {
		return bit18;
	}
	public void setBit18(String bit18) {
		this.bit18 = bit18;
	}
	public String getBit22() {
		return bit22;
	}
	public void setBit22(String bit22) {
		this.bit22 = bit22;
	}
	public String getBit32() {
		return bit32;
	}
	public void setBit32(String bit32) {
		this.bit32 = bit32;
	}
	public String getBit35() {
		return bit35;
	}
	public void setBit35(String bit35) {
		this.bit35 = bit35;
	}
	public String getBit37() {
		return bit37;
	}
	public void setBit37(String bit37) {
		this.bit37 = bit37;
	}
	public String getBit39() {
		return bit39;
	}
	public void setBit39(String bit39) {
		this.bit39 = bit39;
	}
	public String getBit41() {
		return bit41;
	}
	public void setBit41(String bit41) {
		this.bit41 = bit41;
	}
	public String getBit42() {
		return bit42;
	}
	public void setBit42(String bit42) {
		this.bit42 = bit42;
	}
	public String getBit43() {
		return bit43;
	}
	public void setBit43(String bit43) {
		this.bit43 = bit43;
	}
	public String getBit48() {
		return bit48;
	}
	public void setBit48(String bit48) {
		this.bit48 = bit48;
	}
	public String getBit49() {
		return bit49;
	}
	public void setBit49(String bit49) {
		this.bit49 = bit49;
	}
	public String getBit52() {
		return bit52;
	}
	public void setBit52(String bit52) {
		this.bit52 = bit52;
	}
	public String getBit61() {
		return bit61;
	}
	public void setBit61(String bit61) {
		this.bit61 = bit61;
	}
	public String getBit62() {
		return bit62;
	}
	public void setBit62(String bit62) {
		this.bit62 = bit62;
	}
	public String getBit63() {
		return bit63;
	}
	public void setBit63(String bit63) {
		this.bit63 = bit63;
	}
	public String getBit112() {
		return bit112;
	}
	public void setBit112(String bit112) {
		this.bit112 = bit112;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getStrmProp() {
		return strmProp;
	}
	public void setStrmProp(String strmProp) {
		this.strmProp = strmProp;
	}
	public String getMTI() {
		return MTI;
	}
	public void setMTI(String mTI) {
		MTI = mTI;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public String getBit113() {
		return bit113;
	}
	public void setBit113(String bit113) {
		this.bit113 = bit113;
	}
	public String getBit114() {
		return bit114;
	}
	public void setBit114(String bit114) {
		this.bit114 = bit114;
	}
	public String getBit115() {
		return bit115;
	}
	public void setBit115(String bit115) {
		this.bit115 = bit115;
	}
	public String getBit90() {
		return bit90;
	}
	public void setBit90(String bit90) {
		this.bit90 = bit90;
	}
	
}
