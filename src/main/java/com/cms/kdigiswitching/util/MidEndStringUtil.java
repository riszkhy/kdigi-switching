package com.cms.kdigiswitching.util;

public final class MidEndStringUtil 
{
	public final static int COMPARE_EQUAL=1;
	public final static int COMPARE_NOTEQUAL=2;
	public static String rightMostChar(String s,int len,boolean excluded){
		byte[] data = s.getBytes();
		int counter=0;
		int datalength = data.length;
		int n=len;
		char[] buf=new char[len];
		boolean bStart=false;
		for (int i =datalength;i > 0; i--){
			if (excluded){
				if (!bStart){
					bStart=true;
				}
				else {
					buf[--n]=(char)data[i-1];
					counter++;
				}
			}
			else {
				buf[--n]=(char) data[i-1];
				counter++;
			}			
			if (counter == len){
				break;
			}
		}
		return new String(buf);
	}	
    public static String padRight (final String s, final String pad,final int len) {
        StringBuffer d = new StringBuffer(s);
        while (d.length() < len)
            d.append(pad);
        return d.toString();
    }
    public static String left(final String s, final int len){
    	String result = "";
    	if (s!=null){
    		if (s.length() > len){
    			result = s.substring(0,len);
    		}
    		else {
    			result = s;
    		}
    	}
    	return result;
    }
    public static String mid(final String s,final int pos,final int len){
    	return s == null ? null : s.substring(pos-1, pos+len-1);
    }
    public static String mid(final String s,int pos){
    	return s==null ? null : s.substring(pos-1); 
    }
    public static String right(final String s,final int len){
    	return s==null ? null : s.substring(s.length()-len);
    }
    public static int instr(final String s,final String find,final int pos){
    	return s == null ? null : s.indexOf(find,pos-1)+1;
    }
    public static int instr(final String s,final char find,final int pos){
    	return s == null ? null : s.indexOf(find,pos-1)+1;
    }
    public static int instr(final String s,final String find){
    	return instr(s, find,1);
    }
    public static int instr(final String s,final char find){
    	return instr(s, find,1);
    }
    /*
	public static void main(String[] arg){
		String s = "F0F0F0F0F0111111";
		String c = right(s,6);
		System.out.println(c);
	}
	*/
	public static String[] redim(final String[] source,final int len){
        String[] result = new String[len];
        int srclen = source.length;
        if (srclen > len){
        	System.arraycopy(source, 0, result, 0, len);
        }
        else {
        	System.arraycopy(source, 0, result, 0, source.length);	
        }
        return  result;
	}
	public static String[] split(final String source,final String delimiter){
		String[] result = new String[0];		
		boolean stop=false;
		int pos=0;
		int start=1;
		int counter =1;
		while (!stop){
			pos = MidEndStringUtil.instr(source, delimiter,start);
			if (pos==0){
				stop=true;
				if (start >1){
					result=MidEndStringUtil.redim(result, counter);
					result[counter-1]=MidEndStringUtil.mid(source, start);;
					counter++;
				}
				break;
			}
			else {
				result=MidEndStringUtil.redim(result, counter);
				result[counter-1]=MidEndStringUtil.mid(source, start,pos-start);
				counter++;
			}
			start = pos + 1;
		}
		if (counter==1){
			result=MidEndStringUtil.redim(result, counter);
			result[counter-1]=MidEndStringUtil.mid(source, start);
		}
		return result;
	}
	public static String[] split(final String source,final char delimiter){
		String[] result = new String[0];		
		boolean stop=false;
		int pos=0;
		int start=1;
		int counter =1;
		while (!stop){
			pos = MidEndStringUtil.instr(source, delimiter,start);
			if (pos==0){
				stop=true;
				if (start >1){
					result=MidEndStringUtil.redim(result, counter);
					result[counter-1]=MidEndStringUtil.mid(source, start);;
					counter++;
				}
				break;
			}
			else {
				result=MidEndStringUtil.redim(result, counter);
				result[counter-1]=MidEndStringUtil.mid(source, start,pos-start);
				counter++;
			}
			start = pos + 1;
		}
		if (counter==1){
			result=MidEndStringUtil.redim(result, counter);
			result[counter-1]=MidEndStringUtil.mid(source, start);
		}
		return result;
	}	
	public static int compare(final String src,final String dest){
		int result = COMPARE_NOTEQUAL;
		if (src == null){
			if (dest == null){
				result = COMPARE_EQUAL;
			}
		}
		else if (src.equals(dest)){
			result = COMPARE_EQUAL;
		}
		return result;
	}
	
    public static String strpad(final String s, final char pad,final int len) {
        StringBuffer d = new StringBuffer();
        int n = len - s.length();
        while (n-- > 0)
            d.append(pad);
        d.append(s);
        return d.toString();
    }
    public static String strpad1(final String s, final char pad,final int len){
		char[] c= new char[len];
		int n = s.length();
		s.getChars(0, s.length(), c, len-n);
		for (int i = 0; i < len - n;i++){
			c[i]=pad;
		}
		return c.toString();
    }
    public static String concat(final String...source){
    	final StringBuffer buf = new StringBuffer();
    	for (String s : source){
    		buf.append(s);
    	}
    	return buf.toString();
    }
    public static int length(final String s){
    	int result = -1;
    	if (s != null){
    		result = s.length();
    	}
    	return result;
    }
    public static String trim(final String s){
    	String result ="";
    	if (s != null){
    		result = s.trim();
    	}
    	return result;
    }
    public static String reverse(final String source){
    	String result = null;
    	if (source != null){
	    	StringBuilder buffer = new StringBuilder();
	    	for (int i=source.length()-1; i >= 0; i--){
	    		buffer.append(source.charAt(i));
	    	}
	    	result = buffer.toString();
    	}
    	return result;
    }
    public static char[] toCharArray(final String source,boolean reversed){
    	char[] result = new char[source.length()];
    	if (reversed){
        	for (int i = source.length() -1 ; i >= 0 ; i--){
        		result[i] = source.charAt(i);
        	}
    		
    	}
    	else {
	    	for (int i = 0 ; i < source.length(); i++){
	    		result[i] = source.charAt(i);
	    	}
    	}
	    return result;
    }
    public static char[] toCharArray(final String source){
    	return toCharArray(source, false);
    }
    public static String SpaceOrNol(String data,int panjang){
    	int tot=panjang-data.length();
    	String tmpNol = "";
    	for (int i=1; i<=tot; i++){tmpNol+="0";}
    	 	data=tmpNol + data;
    	return data;
        }
	public static String SpaceOrCut(String data,int panjang){		
	   	int tot=panjang-data.length();
	   	for (int i=1;i<=tot;i++){data+=" ";}
	   	if (tot<0) data=data.substring(0,panjang);
	   	return data;
	 }
	
	public  String removeLeadingZeros(String str) {
        if (str == null) {
            return null;
        }
        char[] chars = str.toCharArray();
        int index = 0;
        for (; index < str.length(); index++) {
            if (chars[index] != '0') {
                break;
            }
        }
        return (index == 0) ? str : str.substring(index);
    }
}
